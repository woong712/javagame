import java.awt.*;

import javax.swing.ImageIcon;
import java.util.ArrayList;

class Background
{
	int pw;
	int lastframe;
	Image img;

	ArrayList<String> strings;

	Background()
	{
		try{
        img = (new ImageIcon(this.getClass().getResource("background.jpg"))).getImage();
	    }
	    catch(Exception e)
	    {
	    	System.out.println(e+"cannot load background");
	    }
	}

	void render(Graphics g,int frame)
	{
		pw *= 0.8;
		pw = pw<=0?0:pw;
		{
			float size = pw/1000f+1;
			//g.drawImage(img,0,0,400,400,Board.dis);
        	int c = Board.random.nextInt(768);
			int r_=255-(c<256?255-c:c>=512?c-512:0),
            g_=255-(c<512?c-256:c<=512?512-c:0),
            b_=255-(c>256?c<=512?c-256:767-c:0);
            g.drawImage(img, (int)(0-(Board.B_WIDTH*(size-1))/2), (int)(0-(Board.B_HEIGHT*(size-1))/2) ,(int)(Board.B_WIDTH*size),(int)(Board.B_HEIGHT*size),Board.dis);
        	try{
	        	g.setColor(
		            new Color(
		                r_>255?255:r_,
		                g_>255?255:g_<0?0:g_,
		                b_>255?255:b_,
		                50
		                ));
					g.fillRect(0,0,800,800);
        	}
        	catch(Exception e){
        		g.setColor(new Color(0,0,0,100));
        		g.fillRect(0,0,800,800);
        	}
			
		}	
		
		lastframe = frame;
	}
	void boom(){pw=1000;}	
}